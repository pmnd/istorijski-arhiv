<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:prop="http://www.parlament.gov.rs/propisi"  version="2.0">
    <xsl:template match="/">
		<div>
				<style type="text/css">
                    body { font-family: sans-serif; }
                    p { text-indent: 30px ; text-align:center }
					h1, h2, h3, h4, h5, h6 { text-align:center }
					p.tekstTacke {text-indent: 100px; text-align: justify; margin: 0cm 7cm 0cm 7cm;}
                    p.tekstStava {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
					p.tekstGlave {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
					p.tekstOdeljka {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
					p.tekstClana {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
					p.opisTacke {text-indent: 100px; text-align: justify; margin: 0cm 7cm 0cm 7cm;}
					p.nazivStava {text-indent: 80px; text-align: justify; margin: 0cm 6cm 0cm 6cm;}
                </style>
				<h2 class="nazivPropisa"><b><xsl:value-of select="prop:propis/prop:naziv" /></b></h2>
				<p class="opisPropisa"><xsl:value-of select="prop:propis/prop:preambula/prop:opis" /></p>
				<xsl:if test="prop:propis/prop:tekstPropisa != ''">
					<xsl:if test="not(prop:propis/prop:tekstPropisa/prop:refer)">
							<p class="tekstGlave"><xsl:value-of select="prop:propis/prop:tekstPropisa" /> </p>
					</xsl:if>
                    <xsl:for-each select="prop:propis/prop:tekstPropisa/prop:refer">
						<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
					</xsl:for-each>
				</xsl:if>

				<xsl:for-each select="prop:propis/prop:sadrzajPropisa/prop:deo">
					<h3 class="nazivDeo"><xsl:value-of select="prop:naziv" /></h3>
					<p class="opisDeo"><xsl:value-of select="prop:opis" /></p>
					<xsl:if test="prop:tekstDela != ''">
						<xsl:if test="not(prop:tekstDela/prop:refer)">
							<p class="tekstDela"><xsl:value-of select="prop:tekstDela" /> </p>
						</xsl:if>
                        <xsl:for-each select="prop:tekstDela/prop:refer">
							<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
						</xsl:for-each>
					</xsl:if>
					<br />
						<xsl:for-each select="prop:sadrzajDela/prop:glava">
							<h3 class="nazivGlave"><xsl:value-of select="prop:naziv" /></h3>
							<h4 class="opisGlave"><xsl:value-of select="prop:opis" /></h4>
							<xsl:if test="prop:tekstGlave != ''">
								<xsl:if test="not(prop:tekstGlave/prop:refer)">
									<p class="tekstGlave"><xsl:value-of select="prop:tekstGlave" /> </p>
								</xsl:if>
                                <xsl:for-each select="prop:tekstGlave/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if> 
							<xsl:for-each select="prop:sadrzajGlave/prop:clan">
								<h4 class="nazivClana"><xsl:value-of select="prop:naziv" /> </h4>
								<h4 class="opisClana"><xsl:value-of select="prop:opis" /> </h4>
								<xsl:if test="prop:tekstClana != ''">
									<xsl:if test="not(prop:tekstClana/prop:refer)">
										<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
									</xsl:if>
									<xsl:for-each select="prop:tekstClana/prop:refer">
										<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="prop:sadrzajClana != ''">
									<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<p class="nazivStava"><xsl:value-of select="prop:naziv"/></p>
										<p class="opisStava"><xsl:value-of select="prop:opis"/></p>
										<xsl:if test="prop:tekstStava != ''">
											<xsl:if test="not(prop:tekstStava/prop:refer)">
												<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
											</xsl:if>
											<xsl:for-each select="prop:tekstStava/prop:refer">
												<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
											</xsl:for-each>
										</xsl:if>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<p class="nazivTacke"><xsl:value-of select="prop:naziv"/></p>
											<p class="opisTacke"><xsl:value-of select="prop:opis"/></p>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
							<xsl:for-each select="prop:sadrzajGlave/prop:odeljak">
								<h4 class="nazivOdeljka"><xsl:value-of select="prop:naziv" /> </h4>
								<h4 class="opisOdeljka"><xsl:value-of select="prop:opis" /> </h4>
								<xsl:if test="prop:tekstOdeljka != ''">
									<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
										<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
									</xsl:if>
									<xsl:for-each select="prop:tekstOdeljka/prop:refer">
										<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
									</xsl:for-each>
								</xsl:if>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
									<h4 class="nazivClana"><xsl:value-of select="prop:naziv" /> </h4>
									<h4 class="opisClana"><xsl:value-of select="prop:opis" /> </h4>
									<xsl:if test="prop:tekstClana != ''">
										<xsl:if test="not(prop:tekstClana/prop:refer)">
											<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
										</xsl:if>
										<xsl:for-each select="prop:tekstClana/prop:refer">
											<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
										</xsl:for-each>
									</xsl:if>
									<xsl:if test="prop:sadrzajClana != ''">
										<xsl:for-each select="prop:sadrzajClana/prop:stav">
											<p class="nazivStava"><xsl:value-of select="prop:naziv"/></p>
											<p class="opisStava"><xsl:value-of select="prop:opis"/></p>
											<xsl:if test="prop:tekstStava != ''">
												<xsl:if test="not(prop:tekstStava/prop:refer)">
													<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
												</xsl:if>
												<xsl:for-each select="prop:tekstStava/prop:refer">
													<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
												</xsl:for-each>
											</xsl:if>
											
											<xsl:for-each select="prop:sadrzajStava/prop:tacka">
												<p class="nazivTacke"><xsl:value-of select="prop:naziv"/></p>
												<p class="opisTacke"><xsl:value-of select="prop:opis"/></p>
												<xsl:if test="prop:tekstTacke != ''">
													<xsl:if test="not(prop:tekstTacke/prop:refer)">
														<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
													</xsl:if>
													<xsl:for-each select="prop:tekstTacke/prop:refer">
														<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
													</xsl:for-each>
												</xsl:if>
											</xsl:for-each>
										</xsl:for-each>
									</xsl:if>
								</xsl:for-each>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
									<h4 class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b> </h4>
									<h4 class="opisOdeljka"><xsl:value-of select="prop:opis" /> </h4>
									<xsl:if test="prop:tekstOdeljka != ''">
										<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
											<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
										</xsl:if>
										<xsl:for-each select="prop:tekstOdeljka/prop:refer">
											<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
										</xsl:for-each>
									</xsl:if>
									<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
										<h4 class="nazivClana"><b><xsl:value-of select="prop:naziv" /></b> </h4>
										<h4 class="opisClana"><xsl:value-of select="prop:opis" /> </h4>
										<xsl:if test="prop:tekstClana != ''">
											<xsl:if test="not(prop:tekstClana/prop:refer)">
												<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
											</xsl:if>
											<xsl:for-each select="prop:tekstClana/prop:refer">
												<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
											</xsl:for-each>
										</xsl:if>
										<xsl:if test="prop:sadrzajClana != ''">
										<xsl:for-each select="prop:sadrzajClana/prop:stav">
											<p class="nazivStava"><xsl:value-of select="prop:naziv"/></p>
											<p class="opisStava"><xsl:value-of select="prop:opis"/></p>
											<xsl:if test="prop:tekstStava != ''">
												<xsl:if test="not(prop:tekstStava/prop:refer)">
													<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
												</xsl:if>
												<xsl:for-each select="prop:tekstStava/prop:refer">
													<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
												</xsl:for-each>
											</xsl:if>
											
											<xsl:for-each select="prop:sadrzajStava/prop:tacka">
												<p class="nazivTacke"><xsl:value-of select="prop:naziv"/></p>
												<p class="opisTacke"><xsl:value-of select="prop:opis"/></p>
												<xsl:if test="prop:tekstTacke != ''">
													<xsl:if test="not(prop:tekstTacke/prop:refer)">
														<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
													</xsl:if>
													<xsl:for-each select="prop:tekstTacke/prop:refer">
														<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
													</xsl:for-each>
												</xsl:if>
											</xsl:for-each>
										</xsl:for-each>
										</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>
							<br /><br /><br /><br /><br /><br /><br /><br />
						</xsl:for-each>		
				</xsl:for-each>
				<br /><br /><br /><br /><br /><br /><br /><br />
				
				<xsl:for-each select="prop:propis/prop:sadrzajPropisa/prop:glava">
					<h3 class="nazivGlave"><xsl:value-of select="prop:naziv" /> </h3>
					<p class="opisGlave"><xsl:value-of select="prop:opis" /> </p>
					<xsl:if test="prop:tekstGlave != ''">
						<xsl:if test="not(prop:tekstGlave/prop:refer)">
							<p class="tekstGlave"><xsl:value-of select="prop:tekstGlave" /> </p>
						</xsl:if>
						<xsl:for-each select="prop:tekstGlave/prop:refer">
							<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
						</xsl:for-each>
					</xsl:if>
					<xsl:for-each select="prop:sadrzajGlave/prop:clan">
							<h4 class="nazivClana"><xsl:value-of select="prop:naziv" /> </h4>
							<h4 class="opisClana"><xsl:value-of select="prop:opis" /> </h4>
							<xsl:if test="prop:tekstClana != ''">
								<xsl:if test="not(prop:tekstClana/prop:refer)">
									<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
								</xsl:if>
								<xsl:for-each select="prop:tekstClana/prop:refer">
									<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
								</xsl:for-each>
							</xsl:if>
							<xsl:if test="prop:sadrzajClana != ''">
								<xsl:for-each select="prop:sadrzajClana/prop:stav">
										<p class="nazivStava"><xsl:value-of select="prop:naziv"/></p>
										<p class="opisStava"><xsl:value-of select="prop:opis"/></p>
										<xsl:for-each select="prop:sadrzajStava/prop:tacka">
											<p class="nazivTacke"><xsl:value-of select="prop:naziv"/></p>
											<p class="opisTacke"><xsl:value-of select="prop:opis"/></p>
											<xsl:if test="prop:tekstTacke != ''">
												<xsl:if test="not(prop:tekstTacke/prop:refer)">
													<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
												</xsl:if>
												<xsl:for-each select="prop:tekstTacke/prop:refer">
													<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
								</xsl:for-each>
							</xsl:if>
					</xsl:for-each>
					<xsl:for-each select="prop:sadrzajGlave/prop:odeljak">
								<h4 class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b> </h4>
								<h4 class="opisOdeljka"><xsl:value-of select="prop:opis" /> </h4>
								<xsl:if test="prop:tekstOdeljka != ''">
									<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
										<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
									</xsl:if>
									<xsl:for-each select="prop:tekstOdeljka/prop:refer">
										<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
									</xsl:for-each>
								</xsl:if>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
									<h4 class="nazivClana"><b><xsl:value-of select="prop:naziv" /></b> </h4>
									<h4 class="opisClana"><xsl:value-of select="prop:opis" /> </h4>
									<xsl:if test="prop:tekstClana != ''">
										<xsl:if test="not(prop:tekstClana/prop:refer)">
											<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
										</xsl:if>
										<xsl:for-each select="prop:tekstClana/prop:refer">
											<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
										</xsl:for-each>
									</xsl:if>
									<xsl:if test="prop:sadrzajClana != ''">
										<xsl:for-each select="prop:sadrzajClana/prop:stav">
											<p class="nazivStava"><b><xsl:value-of select="prop:naziv"/></b></p>
											<p class="opisStava"><xsl:value-of select="prop:opis"/></p>
											<xsl:if test="prop:tekstStava != ''">
												<xsl:if test="not(prop:tekstStava/prop:refer)">
													<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
												</xsl:if>
												<xsl:for-each select="prop:tekstStava/prop:refer">
													<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
												</xsl:for-each>
											</xsl:if>
											<xsl:for-each select="prop:sadrzajStava/prop:tacka">
												<p class="nazivTacke"><b><xsl:value-of select="prop:naziv"/></b></p>
												<p class="opisTacke"><xsl:value-of select="prop:opis"/></p>
												<xsl:if test="prop:tekstTacke != ''">
													<xsl:if test="not(prop:tekstTacke/prop:refer)">
														<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
													</xsl:if>
													<xsl:for-each select="prop:tekstTacke/prop:refer">
														<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
													</xsl:for-each>
												</xsl:if>
											</xsl:for-each>
										</xsl:for-each>
									</xsl:if>
								</xsl:for-each>
								<xsl:for-each select="prop:sadrzajOdeljka/prop:odeljak">
									<h4 class="nazivOdeljka"><b><xsl:value-of select="prop:naziv" /></b> </h4>
									<h4 class="opisOdeljka"><xsl:value-of select="prop:opis" /> </h4>
									<xsl:if test="prop:tekstOdeljka != ''">
										<xsl:if test="not(prop:tekstOdeljka/prop:refer)">
											<p class="tekstOdeljka"><xsl:value-of select="prop:tekstOdeljka" /> </p>
										</xsl:if>
										<xsl:for-each select="prop:tekstOdeljka/prop:refer">
											<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
										</xsl:for-each>
									</xsl:if>
									<xsl:for-each select="prop:sadrzajOdeljka/prop:clan">
										<h4 class="nazivClana"><b><xsl:value-of select="prop:naziv" /></b> </h4>
										<h4 class="opisClana"><xsl:value-of select="prop:opis" /> </h4>
										<xsl:if test="prop:tekstClana != ''">
											<xsl:if test="not(prop:tekstClana/prop:refer)">
												<p class="tekstClana"><xsl:value-of select="prop:tekstClana" /> </p>
											</xsl:if>
											<xsl:for-each select="prop:tekstClana/prop:refer">
												<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
											</xsl:for-each>
										</xsl:if>
										<xsl:if test="prop:sadrzajClana != ''">
											<xsl:for-each select="prop:sadrzajClana/prop:stav">
												<p class="nazivStava"><b><xsl:value-of select="prop:naziv"/></b></p>
												<p class="opisStava"><xsl:value-of select="prop:opis"/></p>
												<xsl:if test="prop:tekstStava != ''">
													<xsl:if test="not(prop:tekstStava/prop:refer)">
														<p class="tekstStava"><xsl:value-of select="prop:tekstStava" /> </p>
													</xsl:if>
													<xsl:for-each select="prop:tekstStava/prop:refer">
														<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
													</xsl:for-each>
												</xsl:if>
												<xsl:for-each select="prop:sadrzajStava/prop:tacka">
													<p class="nazivTacke"><b><xsl:value-of select="prop:naziv"/></b></p>
													<p class="opisTacke"><xsl:value-of select="prop:opis"/></p>
													<xsl:if test="prop:tekstTacke != ''">
														<xsl:if test="not(prop:tekstTacke/prop:refer)">
															<p class="tekstTacke"><xsl:value-of select="prop:tekstTacke" /> </p>
														</xsl:if>
														<xsl:for-each select="prop:tekstTacke/prop:refer">
															<p class="refTacke"> <a href="{prop:refCvor}"><xsl:value-of select="prop:naziv"/></a></p>
														</xsl:for-each>
													</xsl:if>
												</xsl:for-each>
											</xsl:for-each>
										</xsl:if>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>
				</xsl:for-each>
		</div>
    </xsl:template>
</xsl:stylesheet>