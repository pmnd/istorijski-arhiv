package arhiv.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import arhiv.database.SchemaValidationManager;
import arhiv.dto.PropisDto;
import arhiv.model.TPropis;
import arhiv.service.PropisService;
import arhiv.xmlSecurity.DecryptKEK;
import arhiv.xmlSecurity.VerifySignatureEnveloped;
import helpers.SearchManagerXQuery;
import helpers.XSLFOTransformer;

@RestController
@RequestMapping("/arhiv/propis")
public class ArhivRest {
    
	@Autowired
	PropisService propisService;
	
	@RequestMapping(method = RequestMethod.GET)
    public List<PropisDto> getAll() {
		System.out.println("usao u findall kod arhive!");
		List<PropisDto> list = SearchManagerXQuery.getInstance().findPropisDtoCollection("uCelini");
        return list;
    }
	
	
	@RequestMapping(method = RequestMethod.POST)
    public String create(@RequestBody String param) {
		System.out.println("usao u create arhive!");
		
		String stringParam = param;
		
		
		//******************** proba za decrypt ****
		Unmarshaller unmarshaller = SchemaValidationManager.getPropisUnmarshaller();
		//System.out.println("instancirn unmarsall: " + unmarshaller);
		StringReader reader = new StringReader(stringParam);
		//System.out.println("reader: " + reader);
		
		
		File decryptTempFile = new File("decryptFile" + (new Date().getTime()));
		
		try {
			FileWriter fw = new FileWriter(decryptTempFile);
			fw.write(stringParam);
			fw.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		//*********************** PROVERA DA LI JE FAJL POTPISAN!!! ***
		boolean flag = VerifySignatureEnveloped.getInstance().verifyDocumentSignature(decryptTempFile);
		if (flag) {
			System.out.println("ARHIVA: USPESNO POTPISAN PROPIS!");
		} else {
			System.out.println("ARHIVA: -->NEUSPESNO<-- POTPISAN PROPIS!");
		}
		//*************************************************************
		
		
		decryptTempFile = DecryptKEK.getInstance().decryptDocument(decryptTempFile);
		
		
		String stringParamDecrypt = "";
		try {
			stringParamDecrypt = FileUtils.readFileToString(decryptTempFile);
			//System.out.println(stringParamDecrypt);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("izvrsena dekripcija");
		try {
			System.out.println("priprema za citanje objekta");
			TPropis noviPropis = (TPropis) (TPropis)JAXBIntrospector.getValue(unmarshaller.unmarshal(decryptTempFile));
			System.out.println("objekat validno procitan");
			return propisService.create(noviPropis);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//******************************************
		
		//**************************************************
		
		
        return "";
    }
	
	
	@RequestMapping(value = "/pdf/{id}", method = RequestMethod.GET)
	public void getPDFFile(
	    @PathVariable("id") String id, 
	    HttpServletResponse response) {
	    try {
	    	
	    	try {
				XSLFOTransformer xslfot = new XSLFOTransformer();
				try {
					xslfot.generatePDFPropis(propisService.createPropisFileWithId(id));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
		      // get your file as InputStream
		      InputStream is = new FileInputStream(new File("tempFiles/propis"+id+".pdf"));
		      // copy it to response's OutputStream
		      org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
		      response.flushBuffer();
	    } catch (IOException ex) {
	    	ex.printStackTrace();
	    }

	}
	
}
