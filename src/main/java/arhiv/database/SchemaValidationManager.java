package arhiv.database;

import java.io.File;
import java.nio.file.Files;
import java.util.Date;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import arhiv.model.TPropis;

public abstract class SchemaValidationManager {
	
	public static Marshaller getPropisMarshaller() {
		try {
		JAXBContext contextValidation = JAXBContext.newInstance(TPropis.class);
		
		// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
		Marshaller marshaller = contextValidation.createMarshaller();
		
		// Konfiguracija marshaller-a custom prefiks maperom
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
				
		// Podešavanje marshaller-a
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		return marshaller;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Unmarshaller getPropisUnmarshaller() {
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TPropis.class);
			
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaPropisi.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
			return unmarshaller;
			
			} catch (Exception e){
				e.printStackTrace();
				return null;
			}
	}
	
	public static boolean validateObject(TPropis obj) {
		boolean ret = true;
		
		try {
			JAXBContext contextValidation = JAXBContext.newInstance(TPropis.class);
			Unmarshaller unmarshaller = contextValidation.createUnmarshaller();
			
			//@@@@@@ kreiranje pomocnog fajla koji ce se koristiti za marshall
			// Marshaller je objekat zadužen za konverziju iz objektnog u XML model
			Marshaller marshaller = contextValidation.createMarshaller();
			
			// Konfiguracija marshaller-a custom prefiks maperom
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NSPrefixMapper());
			
			// Podešavanje marshaller-a
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			File fileTemp = new File("tempFiles/file" + (new Date().getTime()));
			// Umesto System.out-a, može se koristiti FileOutputStream
			marshaller.marshal(obj, fileTemp);
			//@@@@@@@@@@@@@@@@@@@@
			
			// XML schema validacija
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("./src/main/java/schemas/semaPropisi.xsd"));
	        
			// Podešavanje unmarshaller-a za XML schema validaciju
			unmarshaller.setSchema(schema);
			unmarshaller.setEventHandler(new MyValidationEventHandler());
			
	        // Test: proširiti XML fajl nepostojećim elementom (npr. <test></test>)
			Object o = unmarshaller.unmarshal(fileTemp);
			Object o2 = JAXBIntrospector.getValue(unmarshaller.unmarshal(fileTemp));
			obj = (TPropis) JAXBIntrospector.getValue(unmarshaller.unmarshal(fileTemp));
			Files.deleteIfExists(fileTemp.toPath());
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return ret;
	}
	
}
