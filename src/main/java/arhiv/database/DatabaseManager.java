package arhiv.database;

import java.io.IOException;

import javax.xml.transform.TransformerFactory;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.DatabaseClientFactory.Authentication;
import com.marklogic.client.util.EditableNamespaceContext;

import arhiv.database.Util.ConnectionProperties;

public class DatabaseManager {
	
	private static DatabaseClient client;
	private static TransformerFactory transformerFactory;
	private static DatabaseManager instance;
	
	private static EditableNamespaceContext namespaces;
	
	static {
		////////****setovanje namespace-ova!!!
		namespaces = new EditableNamespaceContext();
		namespaces.put("prop", "http://www.parlament.gov.rs/propisi");
		//************************************
	}
	
	private DatabaseManager(){
		ConnectionProperties props = null;
		
		try {
			props = Util.loadProperties();
		} catch (IOException e) {
			e.printStackTrace();
		}
		client = DatabaseClientFactory.newClient(props.host, props.port, props.database , props.user, props.password, Authentication.DIGEST);
		transformerFactory = TransformerFactory.newInstance();
		System.out.println("******************" + client+"********************");
		
	}
	
	
	public static DatabaseManager getInstance(){
		if(instance == null){
			instance = new DatabaseManager();
			return instance;
		}else
			return instance;
	}
	
	

	public  DatabaseClient getClient() {
		
		return client;
	}

	public static EditableNamespaceContext getNamespaces() {
		System.out.println("hoce namespace-ove!!!");
		return namespaces;
	}


	public  TransformerFactory getTransformerFactory() {
		return transformerFactory;
	}
	
	
	public static void main(String [] args){
		
			DatabaseClient c = DatabaseManager.getInstance().getClient();
			System.out.println(c.toString());
		
	}
}
