package arhiv.dto;

public class PropisDto {
	private String id;
	private String naziv;
	private String potpisnik;
	private String usvojen;
	private boolean disabled;
	
	public PropisDto() {
		
	}

	public PropisDto(String id, String naziv, String potpisnik, String usvojen) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.potpisnik = potpisnik;
		this.usvojen = usvojen;
		this.disabled = false;
	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPotpisnik() {
		return potpisnik;
	}

	public void setPotpisnik(String potpisnik) {
		this.potpisnik = potpisnik;
	}

	public String getUsvojen() {
		return usvojen;
	}

	public void setUsvojen(String usvojen) {
		this.usvojen = usvojen;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	
}
