package arhiv.service;

import java.io.File;
import java.util.List;

import arhiv.dto.PropisDto;
import arhiv.model.TPropis;

public interface PropisService extends CrudService<TPropis> {
	File getFileById(String id);
	List<PropisDto> findAllDto();
	File createPropisFileWithId(String id);
}
