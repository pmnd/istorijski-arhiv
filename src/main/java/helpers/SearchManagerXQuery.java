package helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.eval.EvalResult;
import com.marklogic.client.eval.EvalResultIterator;
import com.marklogic.client.eval.ServerEvaluationCall;

import arhiv.database.DatabaseManager;
import arhiv.dto.PropisDto;
import arhiv.dto.SearchResultsMetaDto;

public class SearchManagerXQuery {
	private static SearchManagerXQuery instance;
	
	private DatabaseClient client;
	
	private SearchManagerXQuery() {
		client = DatabaseManager.getInstance().getClient();
	}
	
	public static SearchManagerXQuery getInstance() {
		if (instance == null) {
			instance = new SearchManagerXQuery();
		}
		return instance;
	}
	
	public List<SearchResultsMetaDto> searchForPropisMethadata(String datumPredlaganja, String datumUsvajanja, String naziv) {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
		
		List<SearchResultsMetaDto> ret = new ArrayList<SearchResultsMetaDto>();
		
		StringBuilder builder = new StringBuilder();
		
		String whereCondition = "where ( ";
		if (!datumPredlaganja.equals("")) {
			whereCondition += "$c/@datumPredlaganja = '" + datumPredlaganja + "' and ";
		}
		if (!datumUsvajanja.equals("")) {
			whereCondition += "$c/@datumUsvajanja = '" + datumUsvajanja + "' and ";
		}
		whereCondition += "contains($c/prop:naziv,'" + naziv +"') ) ";
		
		builder.append("declare namespace prop=\"http://www.parlament.gov.rs/propisi\"; ");
		
		 builder.append("for $c in /prop:propis ");
		 builder.append(whereCondition);
		 //builder.append("return data($c/@id)");
		 //concat($x/Name,' ',$x/LastName)
		 builder.append("return data(concat($c/@id,'@',$c/prop:naziv))");
		 
		 //System.out.println(builder.toString());
		 
		 
		 //*****************************************
		 invoker.xquery(builder.toString());
	     response = invoker.eval();
	     
	     for (EvalResult result : response) {
	    	 //System.out.println(result.getString());
			String x = result.getString();
			SearchResultsMetaDto retPropis = new SearchResultsMetaDto(x.split("@")[0], x.split("@")[1]);
			ret.add(retPropis);
	     }
		 //*****************************************
		
	     //System.out.println("zavrsena pretraga metapodataka");
	     
		return ret;
	}
	
	public List<SearchResultsMetaDto> searchForAmandmanMethadata(String datumPredlaganja, String naziv) {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
		
		List<SearchResultsMetaDto> ret = new ArrayList<SearchResultsMetaDto>();
		
		StringBuilder builder = new StringBuilder();
		
		String whereCondition = "where ( ";
		if (!datumPredlaganja.equals("")) {
			whereCondition += "$c/@datumPredlaganja = '" + datumPredlaganja + "' and ";
		}
		
		whereCondition += "contains($c/aman:naziv,'" + naziv +"') ) ";
		
		builder.append("declare namespace aman=\"http://www.parlament.gov.rs/amandmani\"; ");
		
		 builder.append("for $c in /aman:amandman ");
		 builder.append(whereCondition);
		 //builder.append("return data($c/@id)");
		 //concat($x/Name,' ',$x/LastName)
		 builder.append("return data(concat($c/@id,'@',$c/aman:naziv))");
		 
		 //System.out.println(builder.toString());
		 
		 
		 //*****************************************
		 invoker.xquery(builder.toString());
	     response = invoker.eval();
	     
	     for (EvalResult result : response) {
	    	 //System.out.println(result.getString());
			String x = result.getString();
			SearchResultsMetaDto retAman = new SearchResultsMetaDto(x.split("@")[0], x.split("@")[1]);
			ret.add(retAman);
	     }
		 //*****************************************
		
	     //System.out.println("zavrsena pretraga metapodataka");
	     
		return ret;
	}
	
	public List<PropisDto> findAllPropisDto() {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
        
        List<PropisDto> ret = new ArrayList<PropisDto>();
        
        StringBuilder builder = new StringBuilder();
        
        builder.append("declare namespace prop=\"http://www.parlament.gov.rs/propisi\"; ");
		
		builder.append("for $c in /prop:propis ");
		builder.append("return data(concat($c/@id,'@',$c/prop:naziv,'@',$c/prop:potpisnik,'@',$c/prop:usvojen))");
		 
		//System.out.println(builder.toString());
		 
		 
		//*****************************************
		invoker.xquery(builder.toString());
	    response = invoker.eval();
	     
	    for (EvalResult result : response) {
	    	//System.out.println(result.getString());
			String x = result.getString();
			PropisDto retPropis = new PropisDto(x.split("@")[0], x.split("@")[1], x.split("@")[2], x.split("@")[3]);
			ret.add(retPropis);
	    }
		//*****************************************
        
        return ret;
	}
	
	
	public List<PropisDto> findPropisDtoCollection(String collection) {
		//****************************************************
		ServerEvaluationCall invoker;
		EvalResultIterator response = null;
        // Invoke the query
        invoker = client.newServerEval();
		//****************************************************
        
        List<PropisDto> ret = new ArrayList<PropisDto>();
        
        StringBuilder builder = new StringBuilder();
        
        builder.append("declare namespace prop=\"http://www.parlament.gov.rs/propisi\"; ");
		
		builder.append("for $c in /prop:propis ");
		builder.append("return data(concat($c/@id,'@',$c/prop:naziv,'@',$c/prop:potpisnik,'@',$c/prop:usvojen))");
		 
		//System.out.println(builder.toString());
		 
		 
		//*****************************************
		invoker.xquery(builder.toString());
	    response = invoker.eval();
	     
	    for (EvalResult result : response) {
	    	//System.out.println(result.getString());
			String x = result.getString();
			if ((x.split("@")[3]).equals(collection)) { 
				PropisDto retPropis = new PropisDto(x.split("@")[0], x.split("@")[1], x.split("@")[2], x.split("@")[3]);
				ret.add(retPropis);
			}
	    }
		//*****************************************
        
        return ret;
	}
	
}
