(function (angular) {
    "use strict";
    angular.module('app', ['app.controllers','app.services','ngRoute', 'ngCookies','ngAnimate', 'treasure-overlay-spinner'])
        .config(function ($routeProvider, $locationProvider, $httpProvider) {
            $routeProvider
                .when('/', {
                    redirectTo: '/predlozeniPropisi'
                }).when('/predlozeniPropisi', {
                    templateUrl: 'templates/predlozeniPropisi.html',
                    controller: 'predlozeniPropisiController'
                }).when('/predlozeniPropisi/:idPropisa', {
                    templateUrl: 'templates/propisShow.html',
                    controller: 'predlozeniPropisShowController'
                }).otherwise('/');

        }).run(['$rootScope', function ($rootScope) {

        $rootScope.spinner = {active:false};
        
        Date.prototype.yyyymmdd = function() {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
            var dd  = this.getDate().toString();
            return yyyy +'-'+ (mm[1]?mm:"0"+mm[0]) +'-'+ (dd[1]?dd:"0"+dd[0])+'Z'; // padding
        };
        }]);
        
    
}(angular));
