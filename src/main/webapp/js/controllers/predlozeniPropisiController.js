(function (angular){
    "use strict";

    angular.module('app.predlozeniPropisiController', [])
        .controller('predlozeniPropisiController', function ($scope, $rootScope, propisiService, $location, $sce) {
            console.log("Usao u predlozeniPropisiController");
            $scope.nazivTabele = "Predloženi propisi"
            $scope.sednicaZapoceta = true;
            $scope.predlozeniPropisi = [];
            $rootScope.spinner.active = true; 

       
            propisiService.getAll().success(function(data) {
                $scope.predlozeniPropisi = data;
                $rootScope.spinner.active = false; 
            });
             
            $scope.showItem= function(id){
                $location.path('/predlozeniPropisi/' + id);
            }

             $scope.generatePDF = function(id){
                $rootScope.spinner.active = true; 
                propisiService.generatePDF(id).success(function(data){
                    console.log(data);
                    var file = new Blob([data], {type: 'application/pdf'});
                    var fileURL = URL.createObjectURL(file);
                    $scope.content = $sce.trustAsResourceUrl(fileURL);
                    $rootScope.spinner.active = false; 
                });
            }

            
        });
    
}(angular));