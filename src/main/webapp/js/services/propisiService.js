(function (angular) {
    "use strict";
    angular.module('app.propisiService', [])
        .factory('propisiService', function ($http) {
        
        var retService = {};

        retService.getAll = function () {       
            var req = {
            method: 'GET',
            url: 'http://localhost:8081/arhiv/propis'
            }


            return $http(req);
        };

          retService.getOne = function (id) {       
            var req = {
            method: 'GET',
            url: 'http://localhost:8081/html/arhiv/'+ id,
            }

            return $http(req);
        };

        retService.generatePDF = function(id){   
            var req = {
            method: 'GET',
            url: 'http://localhost:8081/arhiv/propis/pdf/'+id,
            responseType:'arraybuffer'
            }
            return $http(req);
        }

    
    
        return retService;
    });
    
    
}(angular));